/*
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *    WekaDemo.java
 *    Copyright (C) 2009 University of Waikato, Hamilton, New Zealand
 *
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;

import utility.Util;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;
import weka.core.OptionHandler;
import weka.core.Utils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NumericToNominal;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.ReplaceMissingValues;
import weka.filters.unsupervised.instance.Randomize;

/**
 * A little demo java program for using WEKA.<br/>
 * Check out the Evaluation class for more details.
 * 
 * @author FracPete (fracpete at waikato dot ac dot nz)
 * @version $Revision: 5628 $
 * @see Evaluation
 */
public class Main {
    /** the classifier used internally */
    protected FilteredClassifier fc = null;
    protected Classifier m_Classifier = null;

    /** the filter to use */
    protected Filter m_Filter = null;

    /** the training file */
    protected String m_TrainingFile = null;
    /** the test file */
    protected String m_TestFile = null;

    /** the training instances */
    protected Instances m_Training = null;

    /** the test instances */
    protected Instances m_Test = null;

    /** for evaluating the classifier */
    protected Evaluation m_Evaluation = null;
    
    protected String inputFileName = null;

    /**
     * initializes the demo
     */
    public Main() {
        super();
    }

    /**
     * sets the classifier to use
     * 
     * @param name
     *            the classname of the classifier
     * @param options
     *            the options for the classifier
     */
    public void setClassifier(String name, String[] options) throws Exception {
        m_Classifier = Classifier.forName(name, options);
        fc = new FilteredClassifier();
        fc.setClassifier(m_Classifier);
        Remove remove = new Remove();
        remove.setAttributeIndices("1");
        fc.setFilter(remove);
    }

    /**
     * sets the filter to use
     * 
     * @param name
     *            the classname of the filter
     * @param options
     *            the options for the filter
     */
    public void setFilter(String name, String[] options) throws Exception {
        m_Filter = (Filter) Class.forName(name).newInstance();
        if (m_Filter instanceof OptionHandler)
            ((OptionHandler) m_Filter).setOptions(options);
    }

    /**
     * sets the file to use for training
     */
    public void setTraining(String name) throws Exception {
        m_TrainingFile = name;
        m_Training = new Instances(new BufferedReader(new FileReader(
                m_TrainingFile)));
        m_Training.setClassIndex(m_Training.numAttributes() - 1);
    }

    /**
     * sets the file to use for test
     */
    public void setTest(String name) throws Exception {
        m_TestFile = name;
        m_Test = new Instances(new BufferedReader(new FileReader(m_TestFile)));
        m_Test.setClassIndex(m_Test.numAttributes() - 1);
    }

    /**
     * runs 10fold CV over the training file
     */
    public void execute(boolean isEvaluateOnTest) throws Exception {
        // run filter
        Instances[] filtered = this.applyFilters(m_Training, m_Test);
        // train classifier on complete file
        if (isEvaluateOnTest) {
            fc.buildClassifier(filtered[0]); // 0 index has train data
            m_Evaluation = new Evaluation(filtered[0]);
            m_Evaluation.evaluateModel(fc, filtered[1]);
            this.generateOutput(filtered);
        } else {
            // evaluate by crossvalidation
            // 10fold CV with seed=1
            m_Evaluation = new Evaluation(filtered[0]);
            m_Evaluation.crossValidateModel(fc, filtered[0], 5,
                    m_Training.getRandomNumberGenerator(1));
        }
        // m_Classifier.buildClassifier(filtered);

        // SerializationHelper.write("data/dummy.model",m_Classifier);
    }
    public void generateOutput(Instances[] filtered) throws Exception{
        Util.createDirs("output");
        PrintWriter outputWriter = Util.openFile("output/"+this.inputFileName+".txt", false);
        for (int i = 0; i < filtered[1].numInstances(); i++) {
            double pred = fc.classifyInstance(filtered[1].instance(i));
            String id = (int)(filtered[1].instance(i).value(0))+" ";
            Util.writeToFile(outputWriter, id, false);
            String preString = filtered[1].classAttribute().value((int) pred);
            Util.writeToFile(outputWriter, preString, true);
            System.out.print(", actual: "
                    + filtered[1].classAttribute().value(
                            (int) filtered[1].instance(i).classValue()));
            System.out.println(", predicted: "
                    + filtered[1].classAttribute().value((int) pred));
        }
        Util.closeOutputFile(outputWriter);
    }

    public Instances[] applyFilters(Instances instanceTrain,
            Instances instanceTest) throws Exception {
        System.out.println("beginning  " + instanceTrain.numAttributes());
        System.out.println("beginning  " + instanceTest.numAttributes());
        Randomize randomize = new Randomize();
        randomize.setInputFormat(instanceTrain);
        instanceTrain = Filter.useFilter(instanceTrain, randomize);
        instanceTest = Filter.useFilter(instanceTest, randomize);

       /* Remove remove = new Remove();
        remove.setAttributeIndices("1"); // remove id attribute
        remove.setInputFormat(instanceTrain);
        instanceTrain = Filter.useFilter(instanceTrain, remove); // applied
                                                                 // remove
                                                                 // filter
        instanceTest = Filter.useFilter(instanceTest, remove); // applied remove
                                                               // filter
*/
        System.out.println("after removing id attribute selection "
                + instanceTrain.numAttributes());
        System.out.println("after removing id attribute selection"
                + instanceTest.numAttributes());
        ReplaceMissingValues replaceMissingValues = new ReplaceMissingValues();
        replaceMissingValues.setInputFormat(instanceTrain);
        // replace missing values with mean or mode.
        instanceTrain = Filter.useFilter(instanceTrain, replaceMissingValues);
        instanceTest = Filter.useFilter(instanceTest, replaceMissingValues);

        NumericToNominal numericToNominal = new NumericToNominal();
        numericToNominal.setAttributeIndices("last");
        numericToNominal.setInputFormat(instanceTrain);
        instanceTrain = Filter.useFilter(instanceTrain, numericToNominal);
        instanceTest = Filter.useFilter(instanceTest, numericToNominal);
        /*
         * List<Integer> numList = new ArrayList<Integer>();
         * numList.add(8);numList
         * .add(13);numList.add(20);numList.add(63);numList
         * .add(66);numList.add(70);numList.add(71); for(int i=0; i<
         * numList.size();i++){ int index = numList.get(i)-1;
         * System.out.print(instanceTrain.attribute(index).name());
         * System.out.print(" : "+
         * instanceTrain.instance(1).value(index)+",\t");
         * 
         * 
         * }
         */

        /*
         * Normalize normalize = new Normalize();
         * normalize.setInputFormat(instanceTrain); instanceTrain =
         * Filter.useFilter(instanceTrain, normalize); instanceTest =
         * Filter.useFilter(instanceTest, normalize);
         */
        /*
         * AttributeSelection attributeSelection = new AttributeSelection();
         * attributeSelection.setInputFormat(instanceTrain); instanceTrain =
         * Filter.useFilter(instanceTrain, attributeSelection); instanceTest =
         * Filter.useFilter(instanceTest, attributeSelection);
         */
        Instances[] instances = new Instances[2];
        instances[0] = instanceTrain;
        System.out.println("after attribute selection "
                + instanceTrain.numAttributes());
        System.out.println("after attribute selection "
                + instanceTest.numAttributes());
        /*
         * for(int i=0; i< instanceTrain.numAttributes();i++){
         * System.out.print(instanceTrain.attribute(i).name());
         * System.out.print(" : "+ instanceTrain.instance(1).value(i)+",\t");
         * 
         * 
         * }
         */
        // System.exit(1);
        instances[1] = instanceTest;
        return instances;
    }

    /**
     * outputs some data about the classifier
     */
    public String toString() {
        StringBuffer result;

        result = new StringBuffer();
        result.append("Weka - Demo\n===========\n\n");

        result.append("Classifier...: " + m_Classifier.getClass().getName()
                + " " + Utils.joinOptions(m_Classifier.getOptions()) + "\n");
        if (m_Filter instanceof OptionHandler)
            result.append("Filter.......: "
                    + m_Filter.getClass().getName()
                    + " "
                    + Utils.joinOptions(((OptionHandler) m_Filter).getOptions())
                    + "\n");
        else
            // result.append("Filter.......: " + m_Filter.getClass().getName()
            // + "\n");
            result.append("Training file: " + m_TrainingFile + "\n");
        result.append("\n");

        result.append(m_Classifier.toString() + "\n");
        result.append(m_Evaluation.toSummaryString() + "\n");
        try {
            result.append(m_Evaluation.toMatrixString() + "\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            result.append(m_Evaluation.toClassDetailsString() + "\n");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result.toString();
    }

    /**
     * returns the usage of the class
     */
    public static String usage() {
        return "\nusage:\n  " + Main.class.getName()
                + "  CLASSIFIER <classname> [options] \n"
                + "  FILTER <classname> [options]\n"
                + "  DATASET <trainingfile>\n\n" + "e.g., \n"
                + "  java -classpath \".:weka.jar\" WekaDemo \n"
                + "    CLASSIFIER weka.classifiers.trees.J48 -U \n"
                + "    FILTER weka.filters.unsupervised.instance.Randomize \n"
                + "    DATASET iris.arff\n";
    }

    /**
     * runs the program, the command line looks like this:<br/>
     * WekaDemo CLASSIFIER classname [options] FILTER classname [options]
     * DATASET filename <br/>
     * e.g., <br/>
     * java -classpath ".:weka.jar" WekaDemo \<br/>
     * CLASSIFIER weka.classifiers.trees.J48 -U \<br/>
     * FILTER weka.filters.unsupervised.instance.Randomize \<br/>
     * DATASET iris.arff<br/>
     */
    public static void main(String[] args) throws Exception {
        Main demo;

        /*
         * if (args.length < 6) { System.out.println(Main.usage());
         * System.exit(1); }
         * 
         * // parse command line String classifier = ""; String filter = "";
         * String dataset = ""; Vector classifierOptions = new Vector(); Vector
         * filterOptions = new Vector();
         * 
         * int i = 0; String current = ""; boolean newPart = false; do { //
         * determine part of command line if (args[i].equals("CLASSIFIER")) {
         * current = args[i]; i++; newPart = true; } else if
         * (args[i].equals("FILTER")) { current = args[i]; i++; newPart = true;
         * } else if (args[i].equals("DATASET")) { current = args[i]; i++;
         * newPart = true; }
         * 
         * if (current.equals("CLASSIFIER")) { if (newPart) classifier =
         * args[i]; else classifierOptions.add(args[i]); } else if
         * (current.equals("FILTER")) { if (newPart) filter = args[i]; else
         * filterOptions.add(args[i]); } else if (current.equals("DATASET")) {
         * if (newPart) dataset = args[i]; }
         * 
         * // next parameter i++; newPart = false; } while (i < args.length);
         * 
         * // everything provided? if (classifier.equals("") ||
         * filter.equals("") || dataset.equals("")) {
         * System.out.println("Not all parameters provided!");
         * System.out.println(Main.usage()); System.exit(2); }
         */

        // run
        demo = new Main();
        String classifier = "weka.classifiers.functions.MultilayerPerceptron";
        // classifier = "weka.classifiers.functions.SMO";

        // classifier = "weka.classifiers.meta.Bagging";
        classifier = "weka.classifiers.trees.RandomForest";
        // classifier = "weka.classifiers.bayes.NaiveBayes";
        // classifier = "weka.classifiers.functions.VotedPerceptron";
        // classifier = "weka.classifiers.meta.Vote";
        String[] classifierOptions = new String[0];
        String inputfileName = "train80-5000";
        demo.inputFileName = inputfileName;
        String dataset = "data/"+inputfileName;
        String trainDataset = dataset + "-trainsplit.arff";
        String testDataset = dataset + "-testsplit.arff";
        demo.setClassifier(classifier, classifierOptions);
        // demo.setFilter(filter, filterOptions);
        demo.setTraining(trainDataset);
        demo.setTest(testDataset);
        demo.execute(true);
        System.out.println(demo.toString());
    }
}
